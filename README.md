# Embedded Linux From Scratch

## 介绍

Embedded Linux From Scratch (RISC-V + Linux v6.x)

准备文字输出和视频输出。

## 大纲

### RISC-V Linux 基础

1. 实验环境
    1. Linux Lab Disk 简介
2. ISA
    1. RISC-V 简介
3. OS
    1. Linux 简介
4. Linux 常用命令
    1. shell
    2. 环境变量
5. Linux 三剑客
    1. awk
    2. sed
    3. grep
6. 编辑器
    1. nano
    2. vim
    3. VS Code

### 开发入门

1. 开发工具
    1. toolchain
        1. 交叉编译
        2. gcc
        3. gdb
        4. Makefile
        5. GNU Autotools
    2. 二进制工具
        1. readelf
        2. binutils
    3. 追踪和性能评测工具
        1. trace
        2. Valgrind
    4. 模拟器
        1. QEMU
        2. Spike
2. 编程语言
    1. Assembly
    2. C
    3. Rust
3. 版本控制
    1. git
4. SBI
    1. OpenSBI
    2. RustSBI
5. BootLoader
    1. U-Boot
6. 加速构建
    1. Yocto
    2. Buildroot
    3. BusyBox


### 内核开发

1. Linux 内核源码
    1. 源码目录
    2. 编译
    3. 调试
    4. 启动分析
2. Linux 内核开发
    1. 文档
    2. 模块加载
    3. Kbuild
    4. 系统调用
    5. 中断编程
    6. 文件系统
    7. 设备模型
    8. 平台设备驱动
    9. 设备树
    10. Real-Time Linux
    11. 贡献源码

### 驱动开发

1. 驱动子系统
    1. gpio
    2. i2c
    3. spi
    4. uart
    5. pinctrl
    6. interruupt
2. 驱动开发
    1. 字符设备驱动
    2. 块设备驱动
    3. 网络设备驱动
3. 驱动移植
    1. LED
    2. 串口
    3. Key
    4. LCD
    5. Camera
    6. Wi-Fi
    7. BT

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
